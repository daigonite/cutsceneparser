﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2.structure
{
    class CutsceneCommandConditionDefinition : CutsceneCommandDefinition
    {
        public CutsceneCommandConditionDefinition(byte commandID, string commandNames, string commandArgs, byte commandArgCount) : base(commandID, commandNames, commandArgs, commandArgCount)
        {
            isKeyword = true;
        }
    }
}
