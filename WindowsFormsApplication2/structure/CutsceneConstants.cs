﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2.structure
{
    static class CutsceneConstants
    {
        static public UInt16 headerID;

        static private List<CutsceneCommandDefinition> commands;

        static public readonly Byte[] startSection = { 0xE7, 0xE7, 0xE8, 0xE8 };
        static public readonly Byte[] endSection = { 0xEF, 0xEF, 0xE0, 0xE0};

        static public readonly Byte[] startArgsList = { 0xCF, 0xCF, 0xCE, 0xCE};
        static public readonly Byte[] endArgsList = { 0xC7, 0xC7, 0xC0, 0xC0 };

        static public readonly Byte[] startArg = { 0xDF, 0xDF, 0xDE, 0xDE };
        static public readonly Byte[] endArg = { 0xD8, 0xD8, 0xD0, 0xD0 };

        static public readonly Byte[] argSep = { 0x80, 0x81, 0x82, 0x80};
        static public readonly Byte[] argCount = { 0x88, 0x87, 0x89, 0x89 };

        static public readonly Byte[] headerStart = { 0x95, 0x89, 0x95, 0x89 };
        static public readonly Byte[] headerEnd = { 0x9A, 0x87, 0x9A, 0x87 };

        static public readonly Byte[] commandsStart = { 0xAB, 0xBA, 0xAB, 0xBA };
        static public readonly Byte[] commandsEnd = { 0xDE, 0xAD, 0xBE, 0xEF };

        static public readonly Byte[] addressHeaderStart = { 0x69, 0x69, 0x42, 0x00 };
        static public readonly Byte[] addressHeaderEnd = { 0x19, 0x79, 0x07, 0xBB };
        static public readonly Byte[] addressHeaderSeparator = { 0xCA, 0xBB, 0xBE, 0xAC };

        static public readonly Byte[] startfile = { 0x69, 0x69, 0x69, 0x69 };

        static CutsceneConstants()
        {
            commands = new List<CutsceneCommandDefinition>();
            setCommands();
        }

        static private void setCommands()
        {
            headerID = 0;

            //annotations
            //@header
            //start of a new header. has one argument = the name. but it's not compiled.
            //they are compiled in the order the file is written. easy.
            //header is handled differently than other annotes which are basically switches/args for
            //the whole header.
            commands.Add(new CutsceneAnnotationDefinition(0x10, "@header", "header_name", 1));
            //@cocurrent
            //this script runs cocurrently to any scripts below it with the @cocurrent annotation.
            commands.Add(new CutsceneAnnotationDefinition(0x11, "@cocurrent", "",0));
            //@entry
            //this will be the entry point of the script instead of the first script.
            commands.Add(new CutsceneAnnotationDefinition(0x12, "@entry", "", 0));
            //@randomize
            //this will randomize the position this header has in the list.
            //good for jumbling up files for a production release.
            //entry will override it, so don't worry about accidentally
            //randomizing entry point.
            commands.Add(new CutsceneAnnotationDefinition(0x13, "@randomize", "", 0));
            //@background
            //this will keep the main thread running while this header continues
            //to execute.
            commands.Add(new CutsceneAnnotationDefinition(0x14, "@background", "", 0));



            //adding keywords. they are compiled separately.
            //commands.Add(new CutsceneCommandConditionDefinition(0x93, "@header", "", 1));
            commands.Add(new CutsceneCommandConditionDefinition(0x94, "end", "", 0));
            commands.Add(new CutsceneCommandConditionDefinition(0x95, "goto", "header_id", 1)); //if the script has "end" then it just ends.
            commands.Add(new CutsceneCommandConditionDefinition(0x96, "call", "header_id", 1)); //if the script has "end", returns to the calling script.

            //commands
            commands.Add(new CutsceneCommandDefinition(0xFF,"nop,no-op,nothing,donothing,null,skip","",0));

            commands.Add(new CutsceneCommandDefinition(0x81,"dialog,dialogue,characterdialog,dialogcharacter", "character,text_id",2));
            commands.Add(new CutsceneCommandDefinition(0x82,"text,textbox", "textfile_id,text_id",2));
            commands.Add(new CutsceneCommandDefinition(0x83,"wait,stall","time",1));
            commands.Add(new CutsceneCommandDefinition(0x84,"movechar,movecharacter", "obj1,obj1_x,obj1_y,obj1_speed,obj1_sprite,[...]",255));
            commands.Add(new CutsceneCommandDefinition(0x85,"movecam,movecamera", "object_id,speed",2));
            commands.Add(new CutsceneCommandDefinition(0x86,"movecharcam,movecharactercamera", "object_id,speed,obj1,obj1_x,obj1_y,obj1_speed,obj1_sprite,[...]",255));
            commands.Add(new CutsceneCommandDefinition(0x87,"graphic,image,picture,sprite", "asset_name,opacity,foreground_or_background",3));
            commands.Add(new CutsceneCommandDefinition(0x88,"music,song", "song_name,fade_time",2));
            commands.Add(new CutsceneCommandDefinition(0x89,"sound,soundfx,soundeffect", "soundfx_id, looping",2));
            commands.Add(new CutsceneCommandDefinition(0x8A,"battle,checkbattle", "enemy1,enemy2,enemy3,script",4,true));
            commands.Add(new CutsceneCommandDefinition(0x8B,"party,checkparty", "character",1, true));
            commands.Add(new CutsceneCommandDefinition(0x8C,"event,checkevent", "event_type,event_id",2, true));
            commands.Add(new CutsceneCommandDefinition(0x8D,"item,checkitem", "item_id,quantity",2, true));
            commands.Add(new CutsceneCommandDefinition(0x8E,"inventory,checkinventory", "item_id,quantity",2, true));
            commands.Add(new CutsceneCommandDefinition(0x8F,"recruit,addcharacter", "character",1));
            commands.Add(new CutsceneCommandDefinition(0x90,"giveitem,additem", "item_id,quantity",2));
            commands.Add(new CutsceneCommandDefinition(0x91,"eventset,addevent", "event_type,event_id",2));
        }

        static public UInt16 getHeaderID()
        {
            return headerID++;
        }

        static public void resetHeaderID()
        {
            headerID = 0;
        }

        static private byte countArgs(string s)
        {
            string[] strings = s.Split(',');
            return (byte)strings.Length;
        }

        static public CutsceneCommandDefinition setCommandDefinition(byte b)
        {
            CutsceneCommandDefinition def = getCommandByID(b);

            return def;

        }

        static public CutsceneCommandDefinition getDefinitionByText(string s)
        {
            for(int i = 0; i < commands.Count; i++)
            {
                String[] strings = commands[i].commandNames.Split(',');
                for(int j = 0; j < strings.Length; j++)
                {
                    if (strings[j].Contains(s))
                    {
                        return commands[i];
                    }
                }
            }
            return null;
        }

        static public CutsceneCommandDefinition getCommandByID(byte b)
        {
            for (int i = 0; i < commands.Count; i++)
            {
                if (commands[i].commandID == b)
                {
                    return commands[i];
                }
            }

            return null;
        }

        static public byte commandNameToByte(string s)
        {
            for(int i = 0; i < commands.Count; i++)
            {
                if(checkStrings(s, commands[i].commandNames)){ 
                        return commands[i].commandID;
                }
            }

            throw new CutsceneParseException("I don't recognize this command: " + s + "." + Environment.NewLine);
        }

        static private bool checkStrings(string s, string validStrings)
        {
            string[] strings = validStrings.Split(',');
            for(int i = 0; i < strings.Length; i++)
            {
                if (strings[i].Equals(s))
                {
                    return true;
                }
            }

            return false;
        }

    }
}
