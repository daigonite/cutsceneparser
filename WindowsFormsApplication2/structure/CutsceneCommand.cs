﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//represents a command.

namespace WindowsFormsApplication2.structure
{
    class CutsceneCommand
    {
        const UInt16 mod = 79;

        public Header header { get; set; }
        public String verbatim { get; set; }
        public byte commandID { get; set; }
        String commandName { get; set; } //name of the command
        List<string> arguments { get; set; }//arguments of the command as strings

        //for error checking code
        public bool syntax { get; set; }
        public string syntaxMessage { get; set; }
        public byte argCount = 0;
        public byte testArgCount = 0;

        public CutsceneCommandDefinition definition { get; set; }

        //constructor
        public CutsceneCommand(string line)
        {
            verbatim = line;
            setPropertiesFromVerbatim();
        }

        public CutsceneCommand(string commandName, List<string> args, Header header)
        {
            this.commandName = commandName;
            this.arguments = args;
            verbatim = commandName;
            for(int i = 0; i < arguments.Count; i++)
            {
                verbatim += " " + arguments[i];
            }
            this.header = header;
            testBytecode();
        }

        public CutsceneCommand(byte[] bytes)
        {
            //to do: write a byte parser for the cutscene command.
        }

        public CutsceneCommand()
        {
            commandName = "";
            arguments = new List<string>();
        }

        private void testBytecode()
        {
            try
            {
                if (!verbatim.Equals(""))
                {
                    byte[] bytes = ToByteArray();
                    syntax = true;
                    //Console.WriteLine();
                    //Console.WriteLine(BitConverter.ToString(bytes)); //.Replace("-", string.Empty)

                } else
                {
                    syntax = true;
                }
            }
            catch (Exception e)
            {
                syntaxMessage = "Syntax Error:"+ Environment.NewLine + e.Message;
                Console.WriteLine(syntaxMessage);
                syntax= false;
            }
        }

        //converts to bytes for reading
        public byte[] ToByteArray()
        {
            byte[] byteArray = byteArrayBulk();

            return byteArray;//encode(byteArray);
        }

        private byte[] byteArrayBulk()
        {
            syntax = false;
            syntaxMessage = "";
            argCount = 0;

            List<Byte> byteArray = new List<byte>();

            //sets start section
            byteArray.AddRange(CutsceneConstants.startSection);

            //adds method
            byteArray.AddRange(methodSection());
            if (definition != null)
            {
                Console.WriteLine(definition.commandNames);
            }
            //if its not a keyword (such as goto or call)
            //just inserts the args normally.
            if (!definition.isKeyword)
            {
                //add args
                byteArray.AddRange(argumentsSection());
            } else
            {

                //args are parsed specially here
                if (compareDefinition("goto") ||
                    compareDefinition("call"))
                {
                    UInt16 id = 0;
                    if (!isDangling())
                    {
                        //sets start section
                        //byteArray.AddRange(CutsceneConstants.startArgsList);

                        id = (UInt16)header.parent.getIndexOfHeader(getArg(0));
                        byteArray.AddRange(BitConverter.GetBytes(id));

                        //sets end section
                        //byteArray.AddRange(CutsceneConstants.endArgsList);
                    }
                }
            }

            //sets end section
            //byteArray.AddRange(CutsceneConstants.endSection);
            return byteArray.ToArray();
        }

        //super basic bs
        //mode max value is 255
        private byte[] encode(byte[] bytes)
        {
            byte mod_counter = 0;

            for(var i = 0; i < bytes.Length; i++)
            {
                bytes[i] += mod_counter;
                mod_counter++;
                if(mod_counter > mod)
                {
                    mod_counter = 0;
                }
            }

            return bytes;
        }

        private byte[] decode(byte[] bytes)
        {
            byte mod_counter = 0;

            for (var i = 0; i < bytes.Length; i++)
            {
                bytes[i] -= mod_counter;
                mod_counter++;
                if (mod_counter > mod)
                {
                    mod_counter = 0;
                }
            }

            return bytes;
        }

        //methods for converting to bytes.

        private byte[] argumentToByte(string argVal)
        {
            List<Byte> byteArray = new List<byte>();

            //adds arguments
            byteArray.AddRange(CutsceneConstants.argSep); //Adds this separator to indicate the end of an argument.
            byteArray.AddRange(stringToByte(argVal));
            

            return byteArray.ToArray();
        }

        private byte[] argumentCountSection()
        {
            List<Byte> byteArray = new List<byte>();

            byteArray.AddRange(CutsceneConstants.argCount);
            byteArray.Add(argCount);

            return byteArray.ToArray();
        }

        private byte[] argumentsSection()
        {
            List<Byte> byteArray = new List<byte>();
            //sets start section
            //byteArray.AddRange(CutsceneConstants.startArgsList);

            //trim arguments if necessary.

            argCount = (byte)arguments.Count;

            //sets end section
            //byteArray.AddRange(argumentCountSection());

            //test to make sure the argument count is right.
            if (definition != null)
            {
                if(definition.commandArgCount != 255)
                {
                    if(arguments.Count != definition.commandArgCount)
                    {
                        throw new CutsceneParseException("Incorrect number of arguments." + Environment.NewLine + "Requires: " + definition.commandArgCount + ". Currently has: " + arguments.Count + "." + Environment.NewLine + "Required Args: " + definition.commandArgs + Environment.NewLine);
                    }
                }
            }

            //adds arguments
            for(int j = 0; j < arguments.Count; j++)
            {
                byteArray.AddRange(argumentToByte(arguments[j]));
            }

            //byteArray.AddRange(CutsceneConstants.endArgsList);

            return byteArray.ToArray();
        }

        private byte[] methodSection()
        {
            List<Byte> byteArray = new List<byte>();
            try
            {
                //get id value from name...
                commandID = CutsceneConstants.commandNameToByte(commandName);
                definition = CutsceneConstants.setCommandDefinition(commandID);

                //ignores keywords, they are parsed separately.
                byteArray.Add(commandID);
            }
            catch (Exception e)
            {
                throw new CutsceneParseException("Command defining error:"+ Environment.NewLine + e.Message);
            }

            return byteArray.ToArray();
        }

        private byte[] stringToByte(string stringer)
        {
            char[] chars = stringer.ToCharArray();

            List<Byte> byteArray = new List<byte>();

            for(int i = 0; i < chars.Length; i++)
            {
                byteArray.Add((byte)((chars[i])));
            }

            return byteArray.ToArray();
        }

        private void setPropertiesFromVerbatim()
        {
            string[] strings = verbatim.Split(' ');
            commandName = strings[0];
            arguments = new List<string>();
            for (int i = 1; i < strings.Length; i++)
            {
                arguments.Add(strings[i].Trim());
            }

            testBytecode();
        }

        public Boolean compareDefinition(String s)
        {
            if(definition == null) { return false; }
            String def = definition.commandNames;
            if (def.Contains(s))
            {
                return true;
            }
            return false;
        }

        public String getArg(int i)
        {
            if (arguments.Count == 0)
            {
                return "";
            }
            else
            {
                return arguments[Math.Min(Math.Max(0, i), arguments.Count - 1)];
            }
        }

        //is it dangling or does it have a connection to the headerList.
        public Boolean isDangling()
        {
            if (header != null)
            {
                if (header.parent != null)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
