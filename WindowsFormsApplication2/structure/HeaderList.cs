﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2.structure
{
    class HeaderList
    {
        List<Header> headers;
        public String errors { get; set; }
        public String source { get; set; }

        public HeaderList()
        {
            headers = new List<Header>();
        }

        //get binary for all headers.
        public byte[] getBinary()
        {
            //A map of all the addresses for the headers.
            //Will be placed at the end of the binary, as a table.
            List<Byte> bytes = new List<Byte>();
            List<Header> listOfAllHeaders = headers;
            List<UInt32> addressList = new List<UInt32>();

            List<Byte> addressHeader = new List<Byte>();
            int bytesOfStart = 8;
            //address header
            addressHeader.AddRange(CutsceneConstants.addressHeaderStart);

            UInt32 count;
            for (int i = 0; i < listOfAllHeaders.Count; i++)
            {
                if (listOfAllHeaders[i] != null)
                {
                    
                    count = (UInt32)(bytes.Count + bytesOfStart);
                    addressList.Add(count);
                    bytes.AddRange(listOfAllHeaders[i].getCommandsAsByteArray());

                    //build out address header
                    addressHeader.AddRange(addressHeaderPartToBytes(count));
                }
            }

            //add the last address as the address  of this part.
            count = (UInt32)(bytes.Count + bytesOfStart);
            addressHeader.AddRange(addressHeaderPartToBytes(count));
            //address footer
            addressHeader.AddRange(CutsceneConstants.addressHeaderEnd);
            bytes.AddRange(addressHeader);

            List<Byte> start = new List<byte>();

            start.AddRange(CutsceneConstants.startfile);
            start.AddRange(BitConverter.GetBytes(count));

            bytes.InsertRange(0, start);
            return bytes.ToArray();

        }

        private byte[] addressHeaderPartToBytes(UInt32 address)
        {
            List<Byte> bytes = new List<Byte>();

            //separator
            //bytes.AddRange(CutsceneConstants.addressHeaderSeparator);

            bytes.AddRange(UINT32TOBYTE(address));

            return bytes.ToArray();
        }

        private byte[] UINT32TOBYTE(UInt32 u)
        {
            return BitConverter.GetBytes(u);
        }

        //parses the text into just basic line-by-line commands.
        public void parseCommands(string s)
        {
            errors = "";
            headers.Clear();
            List<CutsceneCommand> commands = new List<CutsceneCommand>();

            //first gets a straight list of commands.
            try
            {
                source = s;

                parseCommandsInternal(commands);
                setErrorList(commands);
                //split the commands into their headers.
                parseHeaders(commands);
                //check any gotos or calls to make sure they have the headers set properly
                for(int i = 0; i < headers.Count; i++)
                {
                    headers[i].checkGotos();
                }
            }
            catch (CutsceneParseException e)
            {
                errors += e.Message;
            }
        }

        private void parseHeaders(List<CutsceneCommand> commands)
        {
            CutsceneCommand headerCommand = null;
            List<CutsceneCommand> subcomm = new List<CutsceneCommand>();
            Header header = null;
            bool cocurrentz = false;
            bool backgroundz = false;
            bool entryz = false;
            bool randomizez = false;

            for(int i = 0; i < commands.Count; i++)
            {
                if(headerCommand == null)
                {
                    //if the header command is null, then you MUST have the current command be a 
                    //header, else it will throw an error.
                    if (commands[i].compareDefinition("header"))
                    {
                        headerCommand = commands[i];
                        subcomm = new List<CutsceneCommand>();
                    } else
                    {
                        throw new CutsceneParseException("Syntax error at line " + (i+1) + ": Header must be defined for all blocks.");
                    }
                } else
                {
                    if (commands[i].compareDefinition("end"))
                    {
                        //wraps up the current block
                        header = new Header(subcomm,this);
                        header.headerName = headerCommand.getArg(0);
                        //sets the current header command to null so that the next block must have a header defined.
                        headerCommand = null;
                        subcomm = null;
                        headers.Add(header);
                        if (cocurrentz)
                        {
                            header.cocurrent = cocurrentz;
                        }
                        if (backgroundz)
                        {
                            header.background = backgroundz;
                        }
                        if (randomizez)
                        {
                            header.randomize = randomizez;
                        }
                        if (entryz)
                        {
                            header.entry = entryz;
                        }
                        cocurrentz = false;
                        backgroundz = false;
                        entryz = false;
                        randomizez = false;
                    } else
                    {
                        if (commands[i].definition.internalCommand && !commands[i].compareDefinition("header"))
                        {
                            //annotes
                            if (commands[Math.Max(0, i - 1)].definition.internalCommand)
                            {
                                if (commands[i].compareDefinition("cocurrent"))
                                {
                                    cocurrentz = true;
                                }
                                if (commands[i].compareDefinition("background"))
                                {
                                    backgroundz = true;
                                }
                                if (commands[i].compareDefinition("entry"))
                                {
                                    entryz = true;
                                }
                                if (commands[i].compareDefinition("randomize"))
                                {
                                    randomizez = true;
                                }
                            } else
                            {
                                throw new CutsceneParseException("Syntax error at line " + (i + 1) + ": Annotations must have an annotation before them, except @header");
                            }
                        }
                        else
                        {
                            //otherwise, adds the command to the current list of subcommands
                            subcomm.Add(commands[i]);
                        }
                    }
                }
            }

            if(subcomm != null || headerCommand != null)
            {
                throw new CutsceneParseException("Syntax error: Unexpected end of code. Make sure all headers are closed with end");
            }
        }

        private void parseCommandsInternal(List<CutsceneCommand> commands)
        {
            commands.Clear();
            string holder;

            string[] comm = source.Split(Environment.NewLine.ToCharArray());
            for (int i = 0; i < comm.Length; i++)
            {
                holder = comm[i].Trim();
                if (!holder.Equals(""))
                {
                    commands.Add(new CutsceneCommand(holder));
                }
            }
        }

        public void setErrorList(List<CutsceneCommand> commands)
        {
            errors = "";
            for (int i = 0; i < commands.Count; i++)
            {
                if (!commands[i].syntax)
                {
                    errors += "Line " + (i + 1) + ": " + commands[i].syntaxMessage + Environment.NewLine;
                }
            }
        }

        public Boolean checkIfHeaderExists(String s)
        {
            for(int i =0; i < headers.Count; i++)
            {
                if (headers[i].headerName.Equals(s))
                {
                    return true;
                }
            }
            return false;
        }

        public int getIndexOfHeader(Header h)
        {
            return headers.IndexOf(h);
        }

        public int getIndexOfHeader(String s)
        {
            for(int i = 0; i< headers.Count; i++)
            {
                if (headers[i].headerName.Equals(s))
                {
                    return i;
                }
            }
            return 65535;
        }

    }
}
