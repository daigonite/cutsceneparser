﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2.structure
{
    class CutsceneParseException : Exception
    {
        private int lineNumber;

        protected CutsceneParseException()
          : base()
       { }

        public CutsceneParseException(int value) :
      base(String.Format("Error detected at line {0}, please review...", value))
   {
            lineNumber = value;
        }

        public CutsceneParseException(string message) : base(message)
        {
            lineNumber = 0;
        }

        public CutsceneParseException(int value, string message)
      : base(message)
   {
            lineNumber = value;
        }

        public CutsceneParseException(int value, string message, Exception innerException) :
      base(message, innerException)
   {
            lineNumber = value;
        }

        protected CutsceneParseException(SerializationInfo info,
                                    StreamingContext context)
      : base(info, context)
   { }

    }
}
