﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication2.structure
{
    class CutsceneCommandDefinition
    {

        //just represents a definition of a command
        public string commandNames { get; set; }
        public byte commandID { get; set; }
        public string commandArgs { get; set; }
        public byte commandArgCount { get; set; }

        public bool isKeyword { get; set; }
        public bool canUseIf { get; set; }
        public bool internalCommand {get;set;}

        public CutsceneCommandDefinition(byte commandID, string commandNames, string commandArgs, byte commandArgCount)
        {
            this.commandNames = commandNames;
            this.commandID = commandID;
            this.commandArgs = commandArgs;
            this.commandArgCount = commandArgCount;
            this.canUseIf = false;
            isKeyword = false;
            internalCommand = false;
        }

        public string getFirstName()
        {
            return commandNames.Split(',')[0];
        }


        public CutsceneCommandDefinition(byte commandID, string commandNames, string commandArgs, byte commandArgCount, bool canUseIf)
        {
            this.commandNames = commandNames;
            this.commandID = commandID;
            this.commandArgs = commandArgs;
            this.commandArgCount = commandArgCount;
            this.canUseIf = canUseIf;
            isKeyword = false;
        }
    }
}
