﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//The header contains all the components of the script together, so that they can be referenced easily.

namespace WindowsFormsApplication2.structure
{
    class Header
    {
        public int headerID;
        public String headerName;
        public List<CutsceneCommand> commands;
        public String errors;
        public String source;
        public bool betweenIfElse;
        public bool isSubHeader;
        public HeaderList parent;
        public byte[] bytes;

        //annotes
        //if set to true, they are compiled into the file.
        //except for header of course lol
        public bool cocurrent { get; set; }
        public bool entry { get; set; }
        public bool randomize { get; set; }
        public bool background { get; set; }



        private byte[] getBinaryCommands()
        {
            List<Byte> bytes = new List<Byte>();

            //add header
            bytes.AddRange(commandsHeader());

            //add commands
            bytes.AddRange(this.bytes);

            //add footer
            bytes.AddRange(commandsFooter());

            return bytes.ToArray();
        }


        //commands header
        private byte[] commandsHeader()
        {
            List<Byte> bytes = new List<Byte>();
            bytes.AddRange(CutsceneConstants.commandsStart);
            return bytes.ToArray();
        }

        //commands footer
        private byte[] commandsFooter()
        {
            List<Byte> bytes = new List<Byte>();
            bytes.AddRange(CutsceneConstants.commandsEnd);
            return bytes.ToArray();
        }

        //constructors

        public Header()
        {
            isSubHeader = false;
            commands = new List<CutsceneCommand>();
            errors = "";
            betweenIfElse = false;
            headerID = CutsceneConstants.getHeaderID();
            cocurrent = false;
            entry = false;
            randomize = false;
            background = false;

        }

        public Header(List<CutsceneCommand> commands,HeaderList list)
        {
            parent = list;
            this.commands = commands;
            errors = "";
            betweenIfElse = false;
            for (int i = 0; i < commands.Count; i++)
            {
                commands[i].header = this;
            }
            cocurrent = false;
            entry = false;
            randomize = false;
            background = false;

            //bytes = getCommandsAsByteArray();
        }

        public byte[] getCommandsAsByteArray()
        {
            List<byte> byteArray = new List<byte>();
            try
            {
                
                if (cocurrent)
                {
                    byteArray.Add(CutsceneConstants.getDefinitionByText("@cocurrent").commandID);
                }
                if (background)
                {
                    byteArray.Add(CutsceneConstants.getDefinitionByText("@background").commandID);
                }
                //end of annotes
                byteArray.AddRange(CutsceneConstants.commandsStart);

                //start writing commands
                for (int i = 0; i < commands.Count; i++)
                {
                    byteArray.AddRange(commands[i].ToByteArray());
                   

                }
                byteArray.AddRange(CutsceneConstants.endSection);
            } catch (CutsceneParseException e)
            {
                byteArray.Clear();
            }

            bytes = byteArray.ToArray();

            return byteArray.ToArray();
        }

        public void checkGotos()
        {
            String argCheck;

            for(int i = 0; i < commands.Count; i++)
            {
                if (commands[i].compareDefinition("goto") ||
                       commands[i].compareDefinition("call"))
                {
                    //check to make sure the argument exists
                    if (!parent.checkIfHeaderExists(commands[i].getArg(0)))
                    {
                        throw new CutsceneParseException("Syntax error: Header name " + commands[i].getArg(0) + " does not exist!");
                    }
                }
            }
        }
    }
}
