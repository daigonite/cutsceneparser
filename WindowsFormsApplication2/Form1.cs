﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication2.structure;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        HeaderList header;

        public Form1()
        {
            InitializeComponent();
            header = new HeaderList();
            
        }

        private void updateHeader()
        {
            CutsceneConstants.resetHeaderID();
            header.parseCommands(textboxCode.Text);
            textboxError.Text = header.errors;
        }

        private void textboxCode_Leave(object sender, EventArgs e)
        {
            updateHeader();
        }

        private void textboxCode_MouseDown(object sender, MouseEventArgs e)
        {
            updateHeader();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void buttonBytesAsString_Click(object sender, EventArgs e)
        {
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            Console.WriteLine(Form1.ActiveForm.Height);
            Console.WriteLine(textboxError.Location.Y);
            textboxError.Height = Form1.ActiveForm.Height - textboxError.Location.Y - 50;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textboxCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonSaveText_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void buttonCompileTo_Click(object sender, EventArgs e)
        {
            updateHeader();
            saveFileDialog2.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            // WriteAllText creates a file, writes the specified string to the file,
            // and then closes the file.    You do NOT need to call Flush() or Close().
            System.IO.File.WriteAllText(saveFileDialog1.FileName, textboxCode.Text);

        }

        private void saveFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            byte[] bytes = header.getBinary();
            System.IO.File.WriteAllBytes(saveFileDialog2.FileName, header.getBinary());
        }
    }
}
