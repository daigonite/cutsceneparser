﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textboxError = new System.Windows.Forms.TextBox();
            this.commandsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textboxCode = new System.Windows.Forms.TextBox();
            this.buttonBytesAsString = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.buttonSaveText = new System.Windows.Forms.Button();
            this.buttonCompileTo = new System.Windows.Forms.Button();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.commandsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // textboxError
            // 
            this.textboxError.AcceptsReturn = true;
            this.textboxError.BackColor = System.Drawing.SystemColors.Control;
            this.textboxError.CausesValidation = false;
            this.textboxError.ForeColor = System.Drawing.Color.Red;
            this.textboxError.Location = new System.Drawing.Point(12, 490);
            this.textboxError.MaximumSize = new System.Drawing.Size(603, 700);
            this.textboxError.Multiline = true;
            this.textboxError.Name = "textboxError";
            this.textboxError.ReadOnly = true;
            this.textboxError.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textboxError.Size = new System.Drawing.Size(603, 77);
            this.textboxError.TabIndex = 1;
            // 
            // textboxCode
            // 
            this.textboxCode.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxCode.Location = new System.Drawing.Point(12, 59);
            this.textboxCode.Multiline = true;
            this.textboxCode.Name = "textboxCode";
            this.textboxCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textboxCode.Size = new System.Drawing.Size(592, 425);
            this.textboxCode.TabIndex = 2;
            this.textboxCode.WordWrap = false;
            this.textboxCode.TextChanged += new System.EventHandler(this.textboxCode_TextChanged);
            this.textboxCode.Leave += new System.EventHandler(this.textboxCode_Leave);
            this.textboxCode.MouseDown += new System.Windows.Forms.MouseEventHandler(this.textboxCode_MouseDown);
            // 
            // buttonBytesAsString
            // 
            this.buttonBytesAsString.Location = new System.Drawing.Point(610, 119);
            this.buttonBytesAsString.Name = "buttonBytesAsString";
            this.buttonBytesAsString.Size = new System.Drawing.Size(126, 38);
            this.buttonBytesAsString.TabIndex = 3;
            this.buttonBytesAsString.Text = "Show Bytes As String";
            this.buttonBytesAsString.UseVisualStyleBackColor = true;
            this.buttonBytesAsString.Click += new System.EventHandler(this.buttonBytesAsString_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(610, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Fun stuff:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(610, 75);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 38);
            this.button1.TabIndex = 5;
            this.button1.Text = "Show Bytes";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // buttonSaveText
            // 
            this.buttonSaveText.Location = new System.Drawing.Point(12, 12);
            this.buttonSaveText.Name = "buttonSaveText";
            this.buttonSaveText.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveText.TabIndex = 6;
            this.buttonSaveText.Text = "Save Text";
            this.buttonSaveText.UseVisualStyleBackColor = true;
            this.buttonSaveText.Click += new System.EventHandler(this.buttonSaveText_Click);
            // 
            // buttonCompileTo
            // 
            this.buttonCompileTo.Location = new System.Drawing.Point(93, 12);
            this.buttonCompileTo.Name = "buttonCompileTo";
            this.buttonCompileTo.Size = new System.Drawing.Size(75, 23);
            this.buttonCompileTo.TabIndex = 7;
            this.buttonCompileTo.Text = "Compile To";
            this.buttonCompileTo.UseVisualStyleBackColor = true;
            this.buttonCompileTo.Click += new System.EventHandler(this.buttonCompileTo_Click);
            // 
            // saveFileDialog2
            // 
            this.saveFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog2_FileOk);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 579);
            this.Controls.Add(this.buttonCompileTo);
            this.Controls.Add(this.buttonSaveText);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonBytesAsString);
            this.Controls.Add(this.textboxCode);
            this.Controls.Add(this.textboxError);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
            ((System.ComponentModel.ISupportInitialize)(this.commandsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textboxError;
        private System.Windows.Forms.BindingSource commandsBindingSource;
        private System.Windows.Forms.TextBox textboxCode;
        private System.Windows.Forms.Button buttonBytesAsString;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button buttonSaveText;
        private System.Windows.Forms.Button buttonCompileTo;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
    }
}

